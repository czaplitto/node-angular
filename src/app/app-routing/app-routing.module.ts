import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MemberComponent } from '../member/member.component';
import { MemberAddModalComponent } from '../member-add-modal/member-add-modal.component';
import { FormMode } from '../model/model';

const routes: Routes = [
  { path: '', component: MemberComponent },
  {
    path: 'members/add',
    component: MemberAddModalComponent,
    data: {
      mode: FormMode.Add
    }
  },
  {
    path: "members/edit/:i",
    component: MemberAddModalComponent,
    data: {
      mode: FormMode.Edit
    }
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
