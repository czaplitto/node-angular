import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Member } from '../model/model';
import { MemberRestService } from './member-rest.service';

@Injectable({
  providedIn: 'root'
})
export class MemberService {

  constructor(private userRestService: MemberRestService) { }


  getMembers(): Observable<Array<Member>> {
    return this.userRestService.getMembers();
  }

  addMember(member: Member): Observable<Member> {
    return this.userRestService.addMember(member);
  }

  findMember(i: number): Observable<Member> {
    return this.userRestService.findMember(i)
  }

  changeMember(i: number, member: Member): Observable<Member> {
    return this.userRestService.changeMember(i, member);
  }

  removeMember(i: number): Observable<Array<Member>> {
    return this.userRestService.removeMember(i)
  }

}