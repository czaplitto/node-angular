import { TestBed } from '@angular/core/testing';

import { MemberRestService } from './member-rest.service';

describe('UserRestService', () => {
  let service: MemberRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MemberRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
