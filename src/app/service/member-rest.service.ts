import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Member } from '../model/model';

@Injectable({
  providedIn: 'root'
})
export class MemberRestService {

  constructor(private httpClient: HttpClient) { }

  getMembers(): Observable<Array<Member>> {
    return this.httpClient.get<Array<Member>>('http://localhost:5000/members');
  }

  addMember(member: Member): Observable<Member> {
    return this.httpClient.post<Member>('http://localhost:5000/members', member);
  }

  findMember(i: number): Observable<Member> {
    return this.httpClient.get<Member>(`http://localhost:5000/members/${i}`);
  }

  changeMember(i: number, member: Member): Observable<Member> {
    return this.httpClient.put<Member>(`http://localhost:5000/members/${i}`, member);
  }

  removeMember(i: number): Observable<Array<Member>> {
    const url = `http://localhost:5000/members/${i}`;
    return this.httpClient.delete<Array<Member>>(url);
  }
}
