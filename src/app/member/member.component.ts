import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Member } from '../model/model';
import { MemberService } from '../service/member.service';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.css'],
})
export class MemberComponent implements OnInit, OnDestroy {

  members: Member[] = [];
  member: Member | undefined;
  constructor(
    private memberService: MemberService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getMembers();
  }

  ngOnDestroy() {
  }

  getMembers(): void {
    this.memberService
      .getMembers()
      .subscribe(members => (this.members = members));
  }

  findMember(i: number) {
    this.router.navigate([`/members/edit/${i}`]);
  }

  remove(i: number) {
    this.memberService
      .removeMember(i)
      .subscribe(() => {
        console.log(`Remove member with index number ${i}`);
        this.getMembers();
      });
  }

  openModal() {
    this.router.navigate(['/members/add']);
  }

}