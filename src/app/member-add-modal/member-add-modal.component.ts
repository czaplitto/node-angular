import { Component, ViewEncapsulation, OnInit, OnDestroy, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { FormMode, Member } from '../model/model';
import { MemberService } from '../service/member.service';

@Component({
  selector: 'app-member-add-modal',
  templateUrl: './member-add-modal.component.html',
  styleUrls: ['./member-add-modal.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class MemberAddModalComponent implements OnInit, OnDestroy {
  formMode: FormMode;
  memberForm: FormGroup;
  isSubmitted: boolean = false;
  member: Member | undefined;
  members: Member[] = [];
  selectedNum: number = new Date().getFullYear() - 18;
  nums: Array<number> = [];

  constructor(
    private memberService: MemberService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.formMode = this.route.snapshot.data.mode;
    this.memberForm = new FormGroup({
      'name': new FormControl(null, [Validators.required, Validators.minLength(3)]),
      'surname': new FormControl(null, Validators.required),
      'age': new FormControl(null, [Validators.required, Validators.pattern('[0-9]{4}')]),
    });
  }

  ngOnInit(): void {
    this.nums = Array.from({ length: 90 }, (_, i) => this.selectedNum - i);
    if (this.formMode === FormMode.Edit) {
      this.memberService
        .findMember(this.route.snapshot.params.i)
        .subscribe((member) => {
          this.member = member;
          this.fillForm(this.member)
        });
    }
  }

  ngOnDestroy(): void {
  }

  fillForm(member: Member) {
    this.memberForm.patchValue({ name: member.name, surname: member.surname, age: new Date().getFullYear() - member.age })
  }

  onNumberChange(e: any): void {
    this.selectedNum = e.target.value;
  }

  open(): void {
    document.body.classList.add('app-member-add-modal-open');
  }

  close(): void {
    document.body.classList.remove('app-member-add-modal-open');
  }

  submit() {
    this.isSubmitted = true;
    if (this.formMode === FormMode.Add) {
      if (this.memberForm.valid && new Date().getFullYear() - this.memberForm.value.age >= 18) {
        this.closeModal();
        this.memberService
          .addMember({
            'name': this.memberForm.value.name,
            "surname": this.memberForm.value.surname,
            "age": new Date().getFullYear() - this.memberForm.value.age
          })
          .subscribe(currentMember => { console.log(`Added new member ${currentMember.name}  ${currentMember.surname}.`) });
      }
    } else if (this.formMode === FormMode.Edit) {
      if (this.memberForm.valid && new Date().getFullYear() - this.memberForm.value.age >= 18) {
        this.closeModal();
        this.memberService
          .changeMember(this.route.snapshot.params.i, {
            'name': this.memberForm.value.name,
            "surname": this.memberForm.value.surname,
            "age": new Date().getFullYear() - this.memberForm.value.age
          })
          .subscribe(currentMember => { console.log(`Updated member ${currentMember.name}  ${currentMember.surname}.`) });
      }
    }

  }

  checkAge() {
    return new Date().getFullYear() - this.memberForm.value.age < 18;
  }

  borderName() {
    return this.isSubmitted && this.memberForm.value.name === null;
  }

  borderSurname() {
    return this.isSubmitted && this.memberForm.value.surname === null;
  }

  borderAge() {
    return this.isSubmitted && this.memberForm.value.age === null || (new Date().getFullYear() - this.memberForm.value.age < 18);
  }

  closeModal() {
    this.router.navigate(['']);
  }

}

