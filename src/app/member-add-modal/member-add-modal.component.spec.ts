import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberAddModalComponent } from './member-add-modal.component';

describe('MemberAddModalComponent', () => {
  let component: MemberAddModalComponent;
  let fixture: ComponentFixture<MemberAddModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MemberAddModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberAddModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
