export interface Member {
    name: string;
    surname: string;
    age: number;
}

export enum FormMode {
    Add,
    Edit
}