const express = require("express");
const cors = require("cors")
const app = express();

let corsOptions = {
    origin: ['http://localhost:4200', 'http://localhost:4000']
}

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

app.use(cors(corsOptions))

const port = process.env.Port || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));

app.get("/", (req, res) => {
    res.send("Hello!!!")
})

require("./members.js")(app)