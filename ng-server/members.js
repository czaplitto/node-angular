const members = require("./mock_members");

module.exports = (app) => {
    app.get("/members", (req, res) => {
        res.send(members)
    })

    app.post("/members", (req, res) => {
        members.push(req.body);
        res.status(200).send(req.body);
    })

    app.get("/members/:i", (req, res) => {
        res.send(members[req.params.i]);
    })

    app.put("/members/:i", (req, res) => {
        members[req.params.i] = req.body;
        res.send(members[req.params.i]);
    })

    app.delete("/members/:i", (req, res) => {
        members.splice(req.params.i, 1);
        res.status(200).send(members);
    })
}