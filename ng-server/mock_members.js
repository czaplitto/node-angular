module.exports = [
    {
        name: "Jan",
        surname: "Kowalski",
        age: 34
    },
    {
        name: "Krzysztof",
        surname: "Wojtczak",
        age: 23
    },
    {
        name: "Franciszek",
        surname: "Ojrza",
        age: 57
    },
    {
        name: "Grzegorz",
        surname: "Lato",
        age: 66
    }
]